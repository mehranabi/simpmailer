<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('', 'HomeController@welcome')->name('welcome');
Route::get('locale/{locale}', 'HomeController@locale')->name('locale');

Route::middleware('auth')->group(function () {
    Route::get('home', 'HomeController@home')->name('home');

    Route::get('settings', 'UsersController@settings')->name('settings');
    Route::post('settings', 'UsersController@password')->name('settings.password');

    Route::get('admins', 'AdminsController@index')->name('admins');
    Route::post('admins', 'AdminsController@store')->name('admins.store')->middleware('can:create,App\Models\Admin');
    Route::get('admins/new', 'AdminsController@form')->name('admins.form')->middleware('can:create,App\Models\Admin');
    Route::get('admin/{id}', 'AdminsController@show')->name('admin');

    Route::get('emails', 'EmailsController@index')->name('emails');
    Route::post('emails', 'EmailsController@send')->name('emails.send');
    Route::get('email/{id}', 'EmailsController@show')->name('email');
    Route::get('email/{id}/rendered', 'EmailsController@view')->name('email.view');

    Route::get('subscribers', 'SubscribersController@index')->name('subscribers');
    Route::post('subscribers', 'SubscribersController@subscribe')->name('subscribe');
    Route::get('subscriber/{id}', 'SubscribersController@show')->name('subscriber');
});

