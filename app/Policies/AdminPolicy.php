<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    protected function canCreate(string $level)
    {
        return $level === Admin::OWNER or $level === Admin::MANAGER;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param Admin $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $this->canCreate($admin->level)
            ? $this->allow()
            : $this->deny('You don\'t have enough privileges to create a new admin.');
    }
}
