<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Subscriber extends Model implements MustVerifyEmailContract
{
    use Notifiable, MustVerifyEmail;

    protected $fillable = [
        'first_name', 'last_name', 'email',
    ];

    protected $appends = [
        'full_name',
    ];

    public function emails()
    {
        return $this->hasMany('App\Models\Email', 'receiver_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
}
