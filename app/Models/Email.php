<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'sender_id', 'receiver_id', 'template',
        'subject', 'content',
    ];

    public function sender()
    {
        return $this->belongsTo('App\Models\Admin', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\Subscriber', 'receiver_id');
    }
}
