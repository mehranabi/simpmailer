<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;

class Admin extends User implements MustVerifyEmail
{
    use Notifiable;

    const OWNER = 'owner';
    const MANAGER = 'manager';
    const PUBLISHER = 'publisher';

    protected $fillable = [
        'first_name', 'last_name', 'email',
        'level',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'full_name',
    ];

    public function emails()
    {
        return $this->hasMany('App\Models\Email', 'sender_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
