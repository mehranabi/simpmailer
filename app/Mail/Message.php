<?php

namespace App\Mail;

use App\Models\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Message extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public string $_subject;
    public string $_content;
    public string $_template;
    public string $_sender;

    /**
     * Create a new message instance.
     *
     * @param string $subject
     * @param string $content
     * @param string $template
     * @param string $sender
     */
    public function __construct(string $subject, string $content, string $template, string $sender)
    {
        $this->_subject = $subject;
        $this->_content = $content;
        $this->_template = $template;
        $this->_sender = $sender;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->_subject . ' | ' . $this->_sender)
            ->markdown($this->_template)
            ->with('content', $this->_content);
    }
}
