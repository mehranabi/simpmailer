<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribeRequest;
use App\Models\Subscriber;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SubscribersController extends Controller
{
    /**
     * Index all subscribers.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $subscribers = Subscriber::withCount('emails')
            ->orderBy('first_name')->paginate(10);

        return view('subscriber.index')
            ->with(compact('subscribers'));
    }

    /**
     * Show subscriber with the given ID.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $subscriber = Subscriber::withCount('emails')->findOrFail($id);
        $emails = $subscriber->emails()->paginate(10);

        return view('subscriber.single')
            ->with(compact('subscriber'))
            ->with(compact('emails'));
    }

    /**
     * Add a nee subscriber in system.
     *
     * @param SubscribeRequest $request
     * @return RedirectResponse
     */
    public function subscribe(SubscribeRequest $request)
    {
        $subscriber = new Subscriber($request->validated());
        $subscriber->save();

        return redirect()->back()->with('success', true);
    }
}
