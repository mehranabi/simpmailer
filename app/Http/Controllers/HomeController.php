<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Subscriber;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Show Welcome page.
     *
     * @return Application|Factory|View
     */
    public function welcome()
    {
        return view('welcome');
    }

    public function locale(string $locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    /**
     * Show dashboard.
     *
     * @return Renderable
     */
    public function home()
    {
        $all_subscribers = Subscriber::select(['id', 'first_name', 'last_name'])->orderBy('first_name')->get();
        $subscribers = Subscriber::withCount('emails')->take(10)->get();
        $emails = Email::with(['receiver:id,first_name,last_name,full_name', 'sender:id,first_name,last_name'])
            ->select(['id', 'subject', 'receiver_id', 'sender_id'])->take(10)->get();

        $templates = collect(Storage::disk('views')->files('vendor/maileclipse/templates'))->filter(function ($file) {
            return !Str::contains($file, '_');
        })->map(function ($file) {
            return [
                'name' => Str::ucfirst(Str::between($file, 'templates/', '.blade')),
                'address' => Str::before($file, '.blade.php'),
            ];
        })->all();

        return view('home')
            ->with(compact('all_subscribers'))
            ->with(compact('subscribers'))
            ->with(compact('emails'))
            ->with(compact('templates'));
    }
}
