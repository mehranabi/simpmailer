<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdminRequest;
use App\Models\Admin;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class AdminsController extends Controller
{
    /**
     * Index all admins.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $admins = Admin::withCount('emails')->paginate(10);

        return view('admin.index')
            ->with(compact('admins'));
    }

    /**
     * Show admin with the given ID.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $admin = Admin::withCount('emails')->findOrFail($id);
        $emails = $admin->emails()->paginate(10);

        return view('admin.single')
            ->with(compact('admin'))
            ->with(compact('emails'));
    }

    public function form()
    {
        return view('admin.store');
    }

    public function store(StoreAdminRequest $request)
    {
        if (auth()->user()->level === Admin::MANAGER and $request->level === Admin::MANAGER) {
            return redirect()->back()->with('privileges', true);
        }

        $admin = new Admin($request->validated());
        $admin->password = Hash::make($request->password);
        $admin->save();

        return redirect()->back()->with('success', true);
    }
}
