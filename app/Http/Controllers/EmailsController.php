<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Mail\Message;
use App\Models\Email;
use App\Models\Subscriber;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;
use ReflectionException;

class EmailsController extends Controller
{
    /**
     * Index all emails in the system.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $emails = Email::with(['sender:id,first_name,last_name', 'receiver:id,first_name,last_name'])
            ->paginate(10);

        return view('email.index')
            ->with(compact('emails'));
    }

    /**
     * Show email with the given ID.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $email = Email::findOrFail($id);

        return view('email.single')
            ->with(compact('email'));
    }

    /**
     * Show email with the given ID as rendered view.
     *
     * @param int $id
     * @return string
     * @throws ReflectionException
     */
    public function view(int $id)
    {
        $email = Email::findOrFail($id);

        return (new Message($email->subject, $email->content, $email->template, $email->sender->full_name))
            ->render();
    }

    /**
     * Send email to targets and save it for later use.
     *
     * @param SendEmailRequest $request
     * @return RedirectResponse
     */
    public function send(SendEmailRequest $request)
    {
        $user = auth()->user();

        $subject = $request->subject;
        $content = $request->message;
        $template = $request->template;

        foreach ($request->receivers as $receiver) {
            $subscriber = Subscriber::findOrFail($receiver);

            $email = new Email();
            $email->subject = $subject;
            $email->content = $content;
            $email->template = $template;
            $email->sender_id = $user->id;
            $email->receiver_id = $receiver;
            $email->save();

            Mail::to($subscriber->email)->queue(new Message($subject, $content, $template, $user->full_name));
        }

        return redirect()->back()->with('success', true);
    }
}
