<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Admin::class, 1)->states('owner')->create();
        factory(Admin::class, 3)->states('manager')->create();
        factory(Admin::class, 5)->states('publisher')->create();
    }
}
