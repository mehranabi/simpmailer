## SimpMailer
*SimpMailer* is a simple mailing system, that people can subscribe for mailing list and registered admins can send emails to them in group or single.

Admins can create templates using lovely [MailEclipse](https://github.com/Qoraiche/laravel-mail-editor) package and put their content in MarkDown format.

#### Idea
Thanks to my buddy, [Mahdi](mailto:noorbala7418@gmail.com), it came to my head to re-develop his system. While he was working on his own **BitMail** project for university.

### Tech Stack
- [Laravel](https://laravel.com)
- [Bootstrap](https://getbootstrap.com)
- [jQuery](https://jquery.com)
- [SimpleMDE](https://simplemde.com)
- [SQLite](https://sqlite.org)

### Run
You can run this project just like any other Laravel projects, but if you insist on having instructions, check these steps out:

- Clone the project: `git clone https://gitlab.com/mehranabi/simpmailer`
- Install Composer dependencies: `composer install`
- **Optional** Install NPM dependencies: `npm install`
- **Optional** Build NPM dependencies: `npm run production'
- **Optional** Modify `.env` file if you wish
- Create a `database.sqlite` in *database* directory
- Do migrations: `php artisan migrate`
- Serve the app: `php artisan serve`

Now, you can access SimpMailer at [localhost:8000](http://localhost:8000).

### Contact
[S. Mehran Abghari](milto:mehran.ab80@gmail.com)
- GitLab: [@mehranabi](https://gitlab.com/mehranabi)
- Telegram: [@thisismehran](https://t.me/thisismehran)

### License
MIT
