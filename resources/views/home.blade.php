@extends('layouts.app')
@section('title')
    @lang('Dashboard')
@endsection
@section('head')
    <style>
        .list-group {
            max-height: 500px;
            overflow: auto;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('New Email')</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('email.send') }}">
                            @csrf
                            <div class="form-group">
                                <label for="subject">@lang('Subject')</label>
                                <input class="form-control" id="subject" name="subject" type="text">
                            </div>
                            <div class="form-group">
                                <label for="receivers">@lang('Recipients')</label>
                                <select class="form-control" id="receivers" name="receivers[]" multiple>
                                    @foreach($all_subscribers as $subscriber)
                                        <option value="{{ $subscriber->id }}">{{ $subscriber->full_name }}</option>
                                    @endforeach
                                </select>
                                <small class="form-text text-muted">@lang('Up to 100 subscribers').</small>
                            </div>
                            <div class="form-group">
                                <label for="template">@lang('Template')</label>
                                <select class="form-control" id="template" name="template" dir="ltr">
                                    @foreach($templates as $template)
                                        <option value="{{ $template['address'] }}">{{ $template['name'] }}
                                            ({{ $template['address'] }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="editor">@lang('Message')</label>
                            <div dir="rtl">
                                <textarea id="editor" name="message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">@lang('Send')</button>
                        </form>
                        <div class="mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(session('success'))
                                <div class="alert alert-success">
                                    <p>@lang('Has been successfully sent')!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>@lang('Latest Subscribers')</div>
                        <a href="{{ route('subscribers') }}" class="btn btn-primary">@lang('View All')</a>
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($subscribers as $subscriber)
                                <a href="{{ route('subscriber', ['id' => $subscriber->id]) }}"
                                   class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                    {{ $subscriber->full_name }}
                                    <span class="badge badge-primary badge-pill">{{ $subscriber->emails_count }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>@lang('Latest Emails')</div>
                        <a href="{{ route('emails') }}" class="btn btn-primary">@lang('View All')</a>
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($emails as $email)
                                <a href="{{ route('email', ['id' => $email->id]) }}"
                                   class="list-group-item list-group-item-action">
                                    {{ $email->sender->full_name }}:
                                    <b>{{ $email->subject }}</b> @lang('to')
                                    <em>{{ $email->receiver->full_name }}</em>
                                    <small>({{ \Carbon\Carbon::parse($email->created_at, 'UTC')->toDateTimeString() }}
                                        )</small>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        const md = new SimpleMDE({
            forceSync: true,
        });
    </script>
@endsection
