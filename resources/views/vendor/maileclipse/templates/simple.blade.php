@component('mail::message')

## You've got a new message!
*{{ now('UTC')->toDateTimeString() }} (UTC)*

{!! $content !!}

@endcomponent
