<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      dir="{{ config('app.locales.'.app()->getLocale().'.dir') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name') }}</title>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    @if(config('app.locales.'.app()->getLocale().'.dir') === 'rtl')
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
        <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/rastikerdar/vazir-font@v26.0.2/dist/font-face.css"/>
        <style>
            * {
                font-family: "Vazir FD", sans-serif;
            }
        </style>
    @else
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    @endif

    @yield('head')
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm sticky-top">
        <div class="container">
            <a class="navbar-brand" href="{{ route('welcome') }}">
                {{ config('app.name') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="menu">
                <ul class="navbar-nav">
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">@lang('Dashboard')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('subscribers') }}">@lang('Subscribers')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('emails') }}">@lang('Emails')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admins') }}">@lang('Admins')</a>
                        </li>
                    @endauth
                </ul>
                <div class="ml-auto mr-auto"></div>
                <ul class="navbar-nav">
                    @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown">
                                {{ auth()->user()->full_name }}
                                ({{ __(\Illuminate\Support\Str::ucfirst(auth()->user()->level)) }})
                                <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('settings') }}">
                                    @lang('Settings')
                                </a>
                                <a class="dropdown-item" href="{{ route('admin', ['id' => auth()->user()->id]) }}">
                                    @lang('My Mails')
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}" id="logout-submitter">
                                    @lang('Logout')
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="post"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endauth
                    @if(app()->getLocale() === 'fa')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('locale', ['locale' => 'en']) }}">English</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('locale', ['locale' => 'fa']) }}">فارسی</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
</div>
<script>
    $('#logout-submitter').on('click', function (event) {
        event.preventDefault();
        $('#logout-form').submit();
    });
</script>
@yield('script')
</body>
</html>
