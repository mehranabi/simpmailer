@extends('layouts.app')
@section('title')
    @lang('Settings')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">@lang('Your Information')</div>
                    <div class="card-body">
                        <div class="list-group">
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('First Name')</div>
                                <b>{{ $user->first_name }}</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Last Name')</div>
                                <b>{{ $user->last_name }}</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Email')</div>
                                <b>{{ $user->email }}</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Level')</div>
                                <b>{{ __(\Illuminate\Support\Str::ucfirst($user->level)) }}</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">@lang('Change Password')</div>
                    <div class="card-body">
                        <form method="post">
                            @csrf
                            <div class="form-group">
                                <label for="password">@lang('New Password')</label>
                                <input class="form-control" id="password" name="password" type="password">
                            </div>
                            <button type="submit" class="btn btn-primary">@lang('Submit')</button>
                        </form>
                        @if(session('success'))
                            <div class="alert alert-success mt-3">
                                <p>@lang('Has been successfully changed')!</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
