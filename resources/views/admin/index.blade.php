@extends('layouts.app')
@section('title')
    @lang('Admins')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>@lang('Admins')</div>
                        <a class="btn btn-primary" href="{{ route('admins.form') }}">@lang('Add New')</a>
                    </div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($admins as $admin)
                                <a href="{{ route('admin', ['id' => $admin->id]) }}"
                                   class="list-group-item list-group-item-action d-flex justify-content-between align-items-center {{ $admin->id === auth()->user()->id ? 'active' : '' }}">
                                    <b>{{ $admin->full_name }} {{ $admin->id === auth()->user()->id ? '('.__('You').')' : '' }}</b>
                                    <div>{{ __(\Illuminate\Support\Str::ucfirst($admin->level)) }}</div>
                                    <small>@lang('Registered At'): {{ \Carbon\Carbon::parse($admin->created_at, 'UTC')->toDateTimeString() }}</small>
                                </a>
                            @endforeach
                        </div>
                        <div class="mt-3">
                            {{ $admins->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
