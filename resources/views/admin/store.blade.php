@extends('layouts.app')
@section('title')
    @lang('New Admin')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        @lang('New Admin')
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admins.store') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="fname">@lang('First Name')</label>
                                        <input class="form-control" id="fname" name="first_name"
                                               type="text">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="lname">@lang('Last Name')</label>
                                        <input class="form-control" id="lname" name="last_name"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">@lang('Email Address')</label>
                                        <input class="form-control" id="email" name="email"
                                               type="email">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="level">@lang('Level')</label>
                                        <select class="form-control" id="level" name="level">
                                            @if(auth()->user()->level === \App\Models\Admin::OWNER)
                                                <option value="manager">@lang('Manager')</option>
                                            @endif
                                            <option value="publisher">@lang('Publisher')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="password">@lang('Password')</label>
                                        <input class="form-control" id="password" name="password"
                                               type="password">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="passwordconf">@lang('Password Confirmation')</label>
                                        <input class="form-control" id="passwordconf"
                                               name="password_confirmation" type="password"
                                               placeholder="{{ __('Enter your password again') }}">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">@lang('Subscribe')</button>
                        </form>
                        <div class="mt-3">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(session('success'))
                                <div class="alert alert-success">
                                    <p>@lang('Has been successfully added')!</p>
                                </div>
                            @endif
                            @if(session('privileges'))
                                <div class="alert alert-success">
                                    <p>@lang('You don\'t have enough privileges')!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
