@extends('layouts.app')
@section('title')
    @lang('Subscribers')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('Subscribers')</div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($subscribers as $subscriber)
                                <a href="{{ route('subscriber', ['id' => $subscriber->id]) }}"
                                   class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                    <div>
                                        {{ $subscriber->full_name }}
                                        <span class="badge badge-primary badge-pill">{{ $subscriber->emails_count }}</span>
                                    </div>
                                    <div>@lang('Joined at'): {{ \Carbon\Carbon::parse($subscriber->created_at, 'UTC')->toDateTimeString() }} (UTC)</div>
                                </a>
                            @endforeach
                        </div>
                        <div class="mt-3">
                            {{ $subscribers->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
