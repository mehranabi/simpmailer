@extends('layouts.app')
@section('title')
    @lang('Subscriber')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>{{ $subscriber->full_name }}</b></div>
                    <div class="card-body">
                        <div class="list-group">
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Joined At')</div>
                                <b>{{ \Carbon\Carbon::parse($subscriber->created_at, 'UTC')->toDateTimeString() }} (UTC)</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Emails Count')</div>
                                <b>{{ $subscriber->emails_count }}</b>
                            </a>
                        </div>
                        <hr>
                        <div class="list-group">
                            @foreach($emails as $email)
                                <a href="{{ route('email', ['id' => $email->id]) }}"
                                   class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                    <em>{{ $email->sender->full_name }}</em>
                                    <b>{{ $email->subject }}</b>
                                    <small>{{ \Carbon\Carbon::parse($email->created_at, 'UTC')->toDateTimeString() }} (UTC)</small>
                                </a>
                            @endforeach
                        </div>
                        <div class="mt-2">
                            {{ $emails->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
