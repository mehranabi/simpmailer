<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      dir="{{ config('app.locales.'.app()->getLocale().'.dir') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    @if(config('app.locales.'.app()->getLocale().'.dir') === 'rtl')
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
        <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/rastikerdar/vazir-font@v26.0.2/dist/font-face.css"/>
        <style>
            * {
                font-family: "Vazir FD", sans-serif;
            }
        </style>
    @else
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    @endif

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ route('home') }}">@lang('Dashboard')</a>
            @else
                <a href="{{ route('login') }}">@lang('Login')</a>
            @endauth
            @if(app()->getLocale() === 'fa')
                <a href="{{ route('locale', ['locale' => 'en']) }}">English</a>
            @else
                <a href="{{ route('locale', ['locale' => 'fa']) }}">فارسی</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            {{ config('app.name') }}
        </div>
        <div class="links">
            <a href="https://gitlab.com/mehranabi/simpmailer">GitLab</a>
        </div>
        <hr>
        <div class="mt-5">
            <form method="post" action="{{ route('subscribe') }}">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="fname">@lang('First Name')</label>
                            <input class="form-control form-control-sm" id="fname" name="first_name" type="text">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lname">@lang('Last Name')</label>
                            <input class="form-control form-control-sm" id="lname" name="last_name" type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">@lang('Email Address')</label>
                    <input class="form-control form-control-sm" id="email" name="email" type="email">
                </div>
                <button class="btn btn-primary" type="submit">@lang('Subscribe')</button>
            </form>
            <div class="mt-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>@lang('Have been successfully subscribed')!</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
</body>
</html>
