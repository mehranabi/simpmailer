@extends('layouts.app')
@section('title')
    @lang('Emails')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('Emails')</div>
                    <div class="card-body">
                        <div class="list-group">
                            @foreach($emails as $email)
                                <a href="{{ route('email', ['id' => $email->id]) }}"
                                   class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                    <div><b>@lang('from')</b> {{ $email->sender->full_name }}</div>
                                    <div><b>@lang('to')</b> {{ $email->receiver->full_name }}</div>
                                    <b>{{ $email->subject }}</b>
                                    <small>({{ \Carbon\Carbon::parse($email->created_at, 'UTC')->toDateTimeString() }})</small>
                                </a>
                            @endforeach
                        </div>
                        <div class="mt-3">
                            {{ $emails->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
