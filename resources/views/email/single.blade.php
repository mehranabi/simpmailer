@extends('layouts.app')
@section('title')
    @lang('Email')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>{{ $email->subject }}</b></div>
                    <div class="card-body">
                        <div class="list-group">
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Sender')</div>
                                <b>{{ $email->sender->full_name }}</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Receiver')</div>
                                <b>{{ $email->receiver->full_name }}</b>
                            </a>
                            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                <div>@lang('Sent At')</div>
                                <b>{{ \Carbon\Carbon::parse($email->created_at, 'UTC')->toDateTimeString() }} (UTC)</b>
                            </a>
                        </div>
                        <a class="btn btn-primary mt-3"
                           href="{{ route('email.view', ['id' => $email->id]) }}">@lang('View Rendered')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
